<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/', function (Request $request, Response $response) {
    $response = $response->withRedirect("/messages");
    return $response;
});

$app->get('/messages', function (Request $request, Response $response) {
    $this->logger->info("Messages list");
    $mapper = new MessageMapper($this->db);
    $messages = $mapper->getMessages();

    $response = $this->view->render($response, "messages.phtml", ["messages" => $messages, "router" => $this->router]);
    return $response;
});

$app->post('/messages/new', function (Request $request, Response $response) {
    $this->logger->info("Messages list");
    $data = $request->getParsedBody();
    $message_data = [];
    $message_data['name'] = filter_var($data['name'], FILTER_SANITIZE_STRING);
    $message_data['text'] = filter_var($data['text'], FILTER_SANITIZE_STRING);

    $message = new MessageEntity($message_data);
    $message_mapper = new MessageMapper($this->db);
    $message_mapper->save($message);

    $response = $response->withRedirect("/messages");
    return $response;
})->setName('message-new');

$app->get('/messages/del/{id}', function (Request $request, Response $response, $args) {
    $this->logger->info("Delete message {id}");
    $message_id = (int)$args['id'];
    $message_mapper = new MessageMapper($this->db);
    $message_mapper->delete($message_id);
    return $response->withRedirect('/messages');
})->setName('message-del');
