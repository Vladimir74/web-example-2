<?php

class MessageMapper extends Mapper
{
    public $id;
    public $name;
    public $text;

    public function getMessages() {
        $sql = file_get_contents(__DIR__ . '/sqls/getMessages.sql');
        $res = $this->db->query($sql);

        $results = [];
        while($row = $res->fetch()) {
            $results[] = new MessageEntity($row);
        }
        return $results;
    }

    public function save(MessageEntity $message) {
        $sql = "insert into messages (name, text) values (:name, :text)";
        $db = $this->db->prepare($sql);
        $result = $db->execute([
            "name" => $message->getName(),
            "text" => $message->getText(),
        ]);

        if (!$result) {
            throw new Exception("Не могу сохранить сообщение");
        }
    }

    public function delete($id) {
        $sql = "delete from messages where id = :id";
        $db = $this->db->prepare($sql);
        $result = $db->execute([
           "id" => $id,
        ]);

        if (!$result) {
            throw new Exception("Не могу удалить сообщение");
        }
    }
}