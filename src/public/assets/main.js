function confirmDel(el) {
    if (window.confirm("Вы действительно хотите удалить сообщение?")) {
        window.location.href = el.attributes.href.value;
    }
}