<?php

require '../vendor/autoload.php';

$config = require '../config/main.php';

$app = new \Slim\App(["settings" => $config]);

$container = $app->getContainer();

require '../di/di.php';
require '../routes/web.php';

$app->run();