<?php
return [
    'displayErrorDetails' => true,
    'addContentLengthHeader' => false,

    'db' => [
        'host' => 'db',
        'user' => 'myuser',
        'pass' => 'secret',
        'dbname' => 'mydb',
    ]
];